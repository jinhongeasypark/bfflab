var ExpressRouter = require('express').Router;
var UsersRoutes = require('./users/users-routes');

class Routes {
    constructor()  {
        this.router = ExpressRouter();
        this.usersRoutes = new UsersRoutes();
        this.route();
    }

    route() {
        this.router.use('/users', this.usersRoutes.router);
    }
}

module.exports = Routes;
