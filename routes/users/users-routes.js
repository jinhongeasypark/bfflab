const ExpressRouter = require('express').Router;
const UsersController = require('./users-controller');

class UsersRoutes {
    constructor() {
        this.router = ExpressRouter();
        this.usersController = new UsersController();
        this.route();
    }

    route() {
        this.router.get('/:id', (req, res, next) => { this.usersController.getUserById({ req, res, next }) });
    }
}

module.exports = UsersRoutes;
