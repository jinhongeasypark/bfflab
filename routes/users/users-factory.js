class User {
    constructor({ id, name }) {
        this.id = id;
        this.name = name;
    }
}

class UsersFactory {
    constructor() {

    }

    createUser({ accountResponse }) {
        const isValid = accountResponse && accountResponse.data && accountResponse.data.id && accountResponse.data.name;
        if (!isValid) { return undefined; }
        const id = accountResponse.data.id;
        const name = accountResponse.data.name;
        return new User({ id, name });
    }
}

module.exports = UsersFactory;