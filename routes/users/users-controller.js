const AccountsModule = require('../../modules/accounts-module');
const UsersFactory = require('./users-factory');

class UsersController {
    constructor() {
        this.accountsModule = new AccountsModule;
        this.usersFactory = new UsersFactory();
    }

    async getUserById({ req, res, next }) {
        let id = req.params.id;
        if (!id) { res.status(400).send(); next(); }
        try {
            const accountResponse = await this.accountsModule.getAccountResponse({ id });
            const user = this.usersFactory.createUser({ accountResponse });
            res.status(200).send(user);
            next();
        } catch (error) {
            res.status(500).send(error);
        }
    }
}

module.exports = UsersController;
