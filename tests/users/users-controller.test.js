const { describe, it, beforeEach } = require('mocha');
const { expect } = require('chai');
const { stub, createStubInstance, assert } = require('sinon');

const UsersController = require('../../routes/users/users-controller');
const UsersFactory = require('../../routes/users/users-factory');
const AccountsModule = require('../../modules/accounts-module');

xdescribe('UsersController', () => {
    let usersController;
    let usersFactoryStub;
    let accountsModuleStub;

    let reqStub;
    let resStub;
    let nextStub;

    beforeEach(() => {
        usersController = new UsersController();
        usersFactoryStub = createStubInstance(UsersFactory);
        accountsModuleStub = createStubInstance(AccountsModule);
        usersController.usersFactory = usersFactoryStub;
        usersController.accountsModule = accountsModuleStub;
        reqStub = stub();
        resStub = stub({ status: (statusCode) => {}, send: (responseBody) => {} });
        resStub.status.returns(resStub);
        resStub.send.returns(resStub);
        nextStub = stub();
    });

    describe('getUserById', () => {
        it('calls accountsModule.getAccountResponse with req.params.id', async () => {
            const id = 'abc';
            const req = { params: { id } };
            const res = resStub;
            const next = nextStub;
            await usersController.getUserById({ req, res, next });
            assert.calledWithExactly(accountsModuleStub.getAccountResponse, { id });
        });
        it('calls usersFactory.createUser with accountResponse', async () => {
            const id = 'abc';
            const req = { params: { id } };
            const res = resStub;
            const next = nextStub;
            const accountResponse = {};
            accountsModuleStub.getAccountResponse.returns(accountResponse);
            await usersController.getUserById({ req, res, next });
            assert.calledWithExactly(usersFactoryStub.createUser, { accountResponse });
        });
    });
});