const { describe, it, beforeEach } = require('mocha');
const { expect } = require('chai');
const { stub, createStubInstance, assert } = require('sinon');

const UsersController = require('../../routes/users/users-controller');
const UsersFactory = require('../../routes/users/users-factory');
const AccountsModule = require('../../modules/accounts-module');

describe.only('Users', () => {
    let usersFactoryStub;
    let accountsModuleStub;
    let usersController;
    beforeEach(() => {
        usersFactoryStub = createStubInstance(UsersFactory);
        accountsModuleStub = createStubInstance(AccountsModule);
        usersController = new UsersController();
        // usersController.usersFactory = usersFactoryStub;
        usersController.accountsModule = accountsModuleStub;

        reqStub = stub();
        resStub = stub({ status: (statusCode) => {}, send: (responseBody) => {} });
        resStub.status.returns(resStub);
        resStub.send.returns(resStub);
        nextStub = stub();
    });
    describe('get', () => {
        it('asdf', async () => {
            const id = '123';
            const name = 'name';
            const req = { params: { id } };
            const res = resStub;
            const next = nextStub;
            const expectedUser = { id, name };
            const expectedAccountResponse = { data: expectedUser };
            accountsModuleStub.getAccountResponse.resolves(expectedAccountResponse);
            const user = await usersController.getUserById({ req, res, next });
            assert.calledWith(resStub.send, expectedUser);
        });
    });
});