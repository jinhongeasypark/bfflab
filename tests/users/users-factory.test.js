const { describe, it, beforeEach } = require('mocha');
const { expect } = require('chai');
const { stub, createStubInstance, assert } = require('sinon');

const UsersFactory = require('../../routes/users/users-factory');

describe('UsersFactory', () => {
    let usersFactory;
    beforeEach(() => {
        usersFactory = new UsersFactory();
    });
    describe('createUser', () => {
        it('returns undefined if no data on response', () => {
            const accountResponse = {};
            const user = usersFactory.createUser({ accountResponse });
            expect(user).to.be.undefined;
        });
        it('returns undefined if no id on data on response', () => {
            const accountResponse = { data: { name: 'name' } };
            const user = usersFactory.createUser({ accountResponse });
            expect(user).to.be.undefined;
        });
        it('returns undefined if no name on data on response', () => {
            const accountResponse = { data: { id: '123' } };
            const user = usersFactory.createUser({ accountResponse });
            expect(user).to.be.undefined;
        });
        it('returns User object if id and name exists on data on response', () => {
            const id = '123';
            const name = 'name';
            const accountResponse = { data: { id, name } };
            const user = usersFactory.createUser({ accountResponse });
            expect(user.id).to.be.equal(id);
            expect(user.name).to.be.equal(name);
        });
    });
});