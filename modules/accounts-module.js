class AccountsModule {
    constructor() {}

    getAccountResponse({ id }) {
        const promise = new Promise((resolve, reject) => {
            const response = {
                data: {
                    id: `${id}`,
                    name: 'John',
                    nickname: 'Johnny'
                }
            };
            resolve(response);
        });
        return promise;
    }
}

module.exports = AccountsModule;